<?php
date_default_timezone_set('America/Sao_Paulo');

switch (getenv('APPLICATION_ENV')) {
    case 'development':
    case 'staging':
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        ini_set('max_execution_time', 600);
        ini_set('display_startup_errors', true);
        ini_set('html_errors', true);
        ini_set('log_errors', true);
        ini_set('track_errors', false);
        break;
    default:
        error_reporting('E_ALL & ~E_DEPRECATED & ~E_STRICT');
        ini_set('max_execution_time', 60);
        ini_set('display_startup_errors', false);
        ini_set('display_errors', false);
        ini_set('html_errors', true);
        ini_set('log_errors', true);
        ini_set('track_errors', false);
}

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

/**
 * Default error handling
 **/
function exceptions_error_handler($severity, $message, $filename, $lineno) 
{
  if (error_reporting() == 0) {
    return;
  }
  if (error_reporting() & $severity) {
    throw new ErrorException($message, E_ERROR, $severity, $filename, $lineno);
  }
}

set_error_handler('exceptions_error_handler');

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
