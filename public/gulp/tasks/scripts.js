var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var config       = require('../config').scripts;

gulp.task('scripts', function () {

    gulp.src(config.src)
        .pipe(browserSync.reload({ stream:true }));

});