var gulp         = require('gulp');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat       = require('gulp-concat');
var browserSync  = require('browser-sync');
var config       = require('../config').sass;
var config_themes_only = require('../config').themes_sass;
var yarg         = require('yargs').argv;

gulp.task('sass', function () {

    gulp.src(config.src)
        .pipe(sass(config.options))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(concat(config.outFilename))
        .pipe(gulp.dest(config.dest))
        .pipe(browserSync.reload({ stream:true }));

});

gulp.task('sass_themes', function () {

    gulp.src(config_themes_only.src)
        .pipe(sass(config_themes_only.options))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(concat(config_themes_only.outFilename))
        .pipe(gulp.dest(config_themes_only.dest));

});

//Uso para gerar um CSS de um tema especifico:
//gulp generator --sassFile=nome_do_tema , por exemplo "anj.direct.predicta.net" sem o prefixo "theme_"
gulp.task('generator', function () {
    gulp.src('./styles/scss/themes/theme_' + yarg.sassFile + '.scss')
        .pipe(sass(config_themes_only.options))
        .pipe(sass({
            outputStyle: 'compressed',
            errLogToConsole: true
        }))
        .pipe(autoprefixer(config.autoprefixer))
        .pipe(concat(yarg.sassFile + '.css'))
        .pipe(gulp.dest(config.generatedThemesDest))
        .pipe(browserSync.reload({ stream:true }));

});


// Usage:
// gulp generatorWatch --sassFile="nome_do_tema_sem_.scss"

gulp.task('generatorWatch', ['browserSync'], function () {
    gulp.watch('./styles/scss/themes/*.scss', ['generator']);
});
