var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var config       = require('../config').markup;

gulp.task('markup', function () {

    gulp.src(config.src)
        .pipe(browserSync.reload({ stream:true }));

});