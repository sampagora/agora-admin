var gulp   = require('gulp');
var config = require('../config');
var browserSync = require('browser-sync');

gulp.task('watch', ['browserSync'], function () {

    gulp.watch(config.sass.src, ['sass']);
    gulp.watch(config.sass.src, ['sass_themes']);

    gulp.watch(config.markup.src, ['markup']);

    gulp.watch(config.scripts.src, ['scripts']);

    // gulp.watch(config.bower.src, ['bower']);

});
