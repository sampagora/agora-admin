module.exports = {

    // Sass task config excluding themes.
    sass: {
        src: ['./styles/scss/**/*.{sass,scss}', '!./styles/scss/themes/theme_*'],
        dest: './styles/css',
        outFilename: 'main.css',
        generatedThemesDest: './styles/css/themes',
        options: {
            errLogToConsole: true,
            outputStyle: 'compressed'
        },
        autoprefixer: {
            browsers: ['last 5 version']
        }
    },

    // Sass task config for all themes.
    themes_sass: {
        src: ['./styles/scss/themes/*.{sass,scss}', './styles/scss/themes/**/.{sass,scss}'],
        dest: './styles/css',
        outFilename: 'main.css',
        generatedThemesDest: './styles/css/themes',
        options: {
            errLogToConsole: true,
            outputStyle: 'compressed'
        },
        autoprefixer: {
            browsers: ['last 5 version']
        }
    },

    // Browser reload
    browserSync: {
        server: {
            baseDir: './'
        }
    },

    // Html config
    markup: {
        src: './*.html'
    },

    scripts: {
        src: './scripts/*.js'
    },

    bower: {
        src: './vendor',
        dest: '../public/vendor'
    }

};
