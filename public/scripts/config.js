var require = {

    baseUrl: '/scripts',

    paths: {
        jquery: '../vendor/jquery/dist/jquery.min',
        jqueryUi: '../vendor/jquery-ui/jquery-ui.min',
        jqueryMask: '../vendor/jQuery-Mask-Plugin/dist/jquery.mask.min',
        foundation: '../vendor/foundation/js/foundation.min',
        foundationTooltip: '../vendor/foundation/js/foundation/foundation.tooltip',
        foundationReveal: '../vendor/foundation/js/foundation/foundation.reveal',
        chosen: '../vendor/chosen/chosen.jquery.min',
        customScroll: '../vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',
        mustache: '../vendor/mustache/mustache.min',
        jstree: '../vendor/jstree/dist/jstree.min',
        jqtree: '../vendor/jqtree/tree.jquery',
        datatables: '../vendor/datatables/media/js/jquery.dataTables',
        jsCookie: '../vendor/js-cookie/src/js.cookie',
        // Application Modules
        formatsModule: 'modules/application/formats/formatsModule'
    },

    shim: {

        foundation: {
            deps: ['jquery'],
            exports: 'foundation'
        },

        foundationTooltip: {
            deps: ['jquery', 'foundation'],
            exports: 'foundationTooltip'
        },

        foundationReveal: {
            deps: ['jquery', 'foundation'],
            exports: 'foundationReveal'
        },

        chosen: {
            deps: ['jquery'],
            exports: 'chosen'
        },

        customScroll: {
            deps: ['jquery'],
            exports: 'customScroll'
        },

        jqueryUi: {
            deps: ['jquery'],
            exports: 'jqueryUi'
        },

        jqueryMask: {
            deps: ['jquery'],
            exports: 'jqueryMask'
        },

        jstree: {
            deps: ['jquery'],
            exports: 'jstree'
        },

        jqtree: {
            deps: ['jquery'],
            exports: 'jqtree'
        },

        datatables: {
            deps: ['jquery']
        }
    }
};
