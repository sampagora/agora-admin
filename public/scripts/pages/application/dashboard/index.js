require([

    'helpers/layout',
    'modules/application/Dashboard/agencyModule'

], function (LayoutHelper, AgencyModule) {

    LayoutHelper.init();
    AgencyModule.index();
    AgencyModule.foundationInit();

});
