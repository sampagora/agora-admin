require([

    'helpers/layout',
    'modules/application/performerModule'

], function (LayoutHelper,PerformerModule) {

    LayoutHelper.init();
    PerformerModule.form();
    PerformerModule.foundationInit();
});
