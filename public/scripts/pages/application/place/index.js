require([

    'helpers/layout',
    'modules/application/placeModule'

], function (LayoutHelper, PlaceModule) {

    LayoutHelper.init();
    PlaceModule.index();
    PlaceModule.foundationInit();

});
