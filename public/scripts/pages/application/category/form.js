require([

    'helpers/layout',
    'modules/application/categoryModule'

], function (LayoutHelper,CategoryModule) {

    LayoutHelper.init();
    CategoryModule.form();
    CategoryModule.foundationInit();
});
