require([

    'helpers/layout',
    'modules/application/categoryModule'

], function (LayoutHelper, CategoryModule) {

    LayoutHelper.init();
    CategoryModule.index();
    CategoryModule.foundationInit();

});
