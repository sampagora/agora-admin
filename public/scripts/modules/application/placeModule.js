define([
    'jquery',
    'jqueryUi',
    'foundation',
    'jqueryMask',
    'helpers/datatable',
    'foundationTooltip',

], function ($, jqueryUi, foundation, jqueryMask, datatableHelper) {
    
    var foundationInit = function () {
        $(document).foundation();
    };

    var index = function () {
        datatableHelper.init();
    };
    
    var form = function () {
        
        $("#form-place").on("submit", function (e) {
            
            if ($('#file')[0].files.length > 0) {
                data.append('file', $('#file')[0].files);
            }else{
                data.append('file', null);
            }

            e.preventDefault();

            $("#place-form-btn").addClass("disabled").attr("disabled", true);

            $.post($(this).attr("action"), $(this).serialize(), function (data) {
               
               window.DirectUI.notificateUser(data.notification,
                   {
                       onSuccess:function(e){
                           alert('aqui');
                          location.href = '/place';
                       },
                       onWarning:function(e){$("#place-form-btn").removeClass("disabled").attr("disabled", false);},
                       onError:function(e){$("#place-form-btn").removeClass("disabled").attr("disabled", false);}
                   }
               );

           }, 'json');

        });

    };
    
    return {
        foundationInit: foundationInit,
        index: index,
        form: form
    };

});
