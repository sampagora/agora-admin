define([
    'jquery',
    'jqueryUi',
    'foundation',
    'jqueryMask',
    'helpers/datatable',
    'foundationTooltip',

], function ($, jqueryUi, foundation, jqueryMask, datatableHelper) {
    
    var foundationInit = function () {
        $(document).foundation();
    };

    var index = function () {
        datatableHelper.init();
    };
    
    var form = function () {
        
        $("#form-category").on("submit", function (e) {

            e.preventDefault();

           $("#category-form-btn").addClass("disabled").attr("disabled", true);

           $.post($(this).attr("action"), $(this).serialize(), function (data) {
               
               window.DirectUI.notificateUser(data.notification,
                   {
                       onSuccess:function(e){
                          location.href = '/category';
                       },
                       onWarning:function(e){$("#category-form-btn").removeClass("disabled").attr("disabled", false);},
                       onError:function(e){$("#category-form-btn").removeClass("disabled").attr("disabled", false);}
                   }
               );

           }, 'json');

        });

    };
    
    $('.delete-category').on('click', function (e) {
        
       e.preventDefault();
        window.DirectUI.confirm(
            {
                element: this,
                message:'Deseja realmente excluir?',
                onSuccess:function(response){
                        $.ajax({
                        method: 'GET',
                        url: $(response.element).data('url'),
                        dataType: 'json'
                    }).done(function (data) {
                        window.DirectUI.notificateUser(data.notification,
                            {
                                onSuccess:function(e){location.reload();}
                            });
                    });
                    return false;
                }
            });

    });
    
    return {
        foundationInit: foundationInit,
        index: index,
        form: form
    };

});
