define([

    'jquery',
    'jqueryUi',
    'foundation',
    'jqueryMask',
    'helpers/datatable',
    'foundationTooltip',

], function ($, jqueryUi, foundation, jqueryMask, datatableHelper) {

    var foundationInit = function () {

        $(document).foundation();

    };

    var index = function () {

        datatableHelper.init({selectors:"#datatable_negotiation,#datatable_creatives,#datatable_pending_creatives,#datatable_auction,#datatable_clients", sort:false, search:false});

    };

    return {
        foundationInit: foundationInit,
        index: index
    };

});
