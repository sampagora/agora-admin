define([

    'jquery',
    'jqueryMask',
    'foundation',
    'foundationReveal'

], function ($) {

    var init = function () {

        loginForm();
        try{localStorage.removeItem('mlastItem');}catch(_y){}

        publisherSignupForm();
        passwordRecoveryForm();
        passwordRecoveryEffect();

        profileActions();
        submitSignUpForm();

        replacePassword();

    };

    var passwordRecoveryEffect = function () {

        $('.login-container .password-recovery')
            .on('click', function () {
                $('.login-container').fadeOut('fast');
                $('.password-recovery-container').fadeIn('fast');
            });

        $('.password-recovery-container .warning')
            .on('click', function () {
                $('.login-container').fadeIn('fast');
                $('.password-recovery-container').fadeOut('fast');
            });

    };

    var loginForm = function () {

        var form = $('form#login');
        var submitButton = form.find('button[type="submit"]');

        form.on('submit', function() {

            submitButton.attr('disabled', 'disabled');

            sendFormData(form, function (data) {

                if (data.status === 0) {
                    notifications({
                        type: '.error',
                        message: data.result
                    });
                } else {
                    notifications({
                        type: '.success',
                        message: 'Você será redirecionado em instantes.'
                    });
                    window.location = '/dashboard';
                }

                submitButton.removeAttr('disabled');

            });

            return false;

        });

    };

    var publisherSignupForm = function () {

        var form = $('form#signup-publisher');
        var submitButton = form.find('button[type="submit"]');

        submitButton.on('click', function (e) {

            e.preventDefault();

            submitButton.attr('disabled', 'disabled');

            sendFormData(form, function (data) {

                if (data.status === 0) {
                    notifications({
                        type: '.error',
                        message: data.result
                    });
                } else {
                    notifications({
                        type: '.success',
                        message: data.result
                    });
                    setTimeout(function() { window.location = '/';}, 3000);
                }

                submitButton.removeAttr('disabled');

            });

        });

    };

    var passwordRecoveryForm = function () {

        var form = $('form#password-recovery');
        var submitButton = form.find('button[type="submit"]');

        submitButton.on('click', function (e) {

            e.preventDefault();

            submitButton.attr('disabled', 'disabled');

            sendFormData(form, function (data) {

                if (data.status === 0) {
                    notifications({
                        type: '.error',
                        message: data.result
                    });
                } else {
                    notifications({
                        type: '.success',
                        message: data.result
                    });
                }

                submitButton.removeAttr('disabled');

            });

        });

    };

    var sendFormData = function (form, callback) {

        $.ajax({
            method: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $('.message.loading').fadeIn();
            },
            complete: function () {
                $('.message.loading').fadeOut();
            },
            success: function (data) {
                callback(data);
            }
        });

    };

    var notifications = function (params) {

        var messageContainer = $('.notifications').find(params.type);
            messageContainer.find('span').html(params.message);
            messageContainer.delay(700).fadeIn();

        setTimeout(function () {
            messageContainer.fadeOut();
        }, 3000);

        $('.notifications .close').on('click', function () {
            $(this).parent().fadeOut();
        });

    };

    var profileActions = function () {

        $('.sign-up-container .profile-advertiser')
            .on('click', function () {

                $('.sign-up-form #perfil').val('anunciante');
                $('.sign-up-form .type').show();
                $('.sign-up-container .step-one').fadeOut();
                $('.sign-up-form').fadeIn();
                $('.sign-up-form h3').eq(0).html('Dados do Anunciante');

                $('.sign-up-form #documento')
                    .mask('00.000.000/0000-00', {reverse: false});

                signUpProfileType();

            });

        $('.sign-up-container .profile-agency')
            .on('click', function () {

                $('.sign-up-form #perfil').val('agencia');
                $('.sign-up-form .type').hide();
                $('.sign-up-container .step-one').fadeOut();
                $('.sign-up-form').fadeIn();
                $('.sign-up-form h3').eq(0).html('Dados da Agência');

                $('.sign-up-form #documento')
                    .mask('00.000.000/0000-00', {reverse: false});

                toCnpj();

            });

        $('.login-container .sign-up')
            .on('click', function () {
                $('.login-container').fadeOut('fast');
                $('.sign-up-container').fadeIn('fast');
                $('.sign-up-container .step-one').fadeIn('fast');
            });

        $('.sign-up-container .warning')
            .on('click', function () {
                $('.sign-up-form').fadeOut('fast');
                $('.sign-up-container').fadeOut('fast');
                $('.login-container').fadeIn('fast');
            });

        $('.sign-up-form input.cep').mask('00000-000');
        $('.sign-up-form input.telefone').mask('(00) 0000-0000');

    };



    var submitSignUpForm = function () {

        var form = $('form#sign-up');

        $('.sign-up-form button.success')
            .on('click', function () {

        $('#create-seller-modal').foundation('reveal', 'open');
        $('#termo_uso_modal').change(function(e){
           if (document.getElementById("termo_uso_modal").checked)
           {
               $("#termo_uso").val("aceito");
               $('#line-item-form-btn').prop("disabled",false);
           }
           else
           {
               $("#termo_uso").val("nao_aceito");
               $('#line-item-form-btn').prop("disabled","disabled");
           }
        });
      });

      $('#line-item-form-btn').off("click").on('click', function () {
      var okTerms = document.getElementById("termo_uso_modal").checked;
      if(okTerms){
            sendFormData(form, function (data) {

                if (data.status === 0) {
                    notifications({
                        type: '.error',
                        message: data.result
                    });
                } else {
                    notifications({
                        type: '.success',
                        message: 'Cadastro realizado com sucesso!'
                    });
                    location.href = '/login';
                }
                return false;

            });
      }else
      if(!okTerms){

        alert('O Termo de Compromisso não foi selecionado');
      }

  });
    };


    var signUpProfileType = function () {

        $('.sign-up-form .type a')
            .on('click', function () {

                if ($(this).data('type') === 'cpf')
                    toCpf();
                else
                    toCnpj();

                $('.type a').removeClass('active');
                $(this).addClass('active');

            });

    };

    var toCpf = function () {

        $('.sign-up-form #ie').hide();

        $('.sign-up-form #documento')
            .attr('name', 'cpf')
            .attr('placeholder', 'CPF')
            .val('');

        $('.sign-up-form #razao_social')
            .attr('placeholder', 'Nome');

        $('.sign-up-form #documento')
            .mask('000.000.000-00', {reverse: false});

    };

    var toCnpj = function () {

        $('.sign-up-form #ie').show();

        $('.sign-up-form #documento')
            .attr('name', 'cnpj')
            .attr('placeholder', 'CNPJ')
            .val('');

            $('.sign-up-form #razao_social')
                .attr('placeholder', 'Razão Social');

        $('.sign-up-form #documento')
            .mask('00.000.000/0000-00', {reverse: false});

    };

    var replacePassword = function () {

        var form = $('form#replace-login');

        form.on('submit', function () {

            sendFormData(form, function (data) {

                if (data.status === 0) {
                    notifications({
                        type: '.error',
                        message: data.result
                    });
                } else {
                    notifications({
                        type: '.success',
                        message: data.result
                    });

                    window.location = '/login';
                }

            });

            return false;

        });

    };

    return {
        init: init
    };

});
