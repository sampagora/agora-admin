define([
    'jquery',
    'jqueryUi',
    'foundation',
    'jqueryMask',
    'helpers/datatable',
    'chosen'

], function ($, jqueryUi, foundation, jqueryMask, datatableHelper, chosen) {
    
    var foundationInit = function () {
        $(document).foundation();
    };

    var index = function () {
        datatableHelper.init();
    };
    
    var form = function () {
        
        $('#category').chosen();
        
        $("#form-performer").on("submit", function (e) {
            
            e.preventDefault();

            $("#performer-form-btn").addClass("disabled").attr("disabled", true);

            $.post($(this).attr("action"), $(this).serialize(), function (data) {

                window.DirectUI.notificateUser(data.notification,
                    {
                        onSuccess:function(e){
                           location.href = '/performer';
                        },
                        onWarning:function(e){$("#performer-form-btn").removeClass("disabled").attr("disabled", false);},
                        onError:function(e){$("#performer-form-btn").removeClass("disabled").attr("disabled", false);}
                    }
                );

            }, 'json');

        });

    };
    
    return {
        foundationInit: foundationInit,
        index: index,
        form: form
    };

});
