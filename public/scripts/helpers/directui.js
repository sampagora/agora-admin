window.DirectUI = {};
window.DirectUI.notificationTimeoutId = 0;

window.DirectUI.notificateUser = function(response, options)
{
    var defaultOptions = {hideVisualFeedback:false,closeTimeout:20, element:undefined}, d = document, msgbox, pannel = "#message-panel", modal_pannel = ".m-message-panel";
    options = $.extend({}, defaultOptions, options);
    options.closeTimeout = Number(options.closeTimeout);
    var fullResponse = false, notification = response, context = d;

    if(typeof response.notification != "undefined")
    {
        fullResponse = true;
        notification = response.notification;
    }

    if(!response || (notification && (!notification.error && !notification.success && !notification.warning && !notification.unknow && !notification.system)))
    {
        //resposta em formato inválido, precisamos fazer alguma coisa, talvez mostrar um warning com uma mensagem genérica
        return;
    }

    //Fecha as msgs antigas antes de exibir novas.
    window.DirectUI.closeNotifications();

    if(options.insideModal && options.insideModal == true)
    {
        pannel = modal_pannel;
        if(options.context)
        {
            context = $(options.context);
        }
    }

    if(!options.hideVisualFeedback || options.hideVisualFeedback === false)
    {
        if(notification.error)
        {

            msgbox = $( pannel + " .error", context);
            msgbox.find('span').text(notification.error.message);
            msgbox.fadeIn();

        }
        if(notification.success)
        {
            msgbox = $( pannel + " .success", context);
            msgbox.find('span').text(notification.success.message);
            msgbox.fadeIn();
        }
        if(notification.warning)
        {
            msgbox = $( pannel + " .warning", context);
            msgbox.find('span').text(notification.warning.message);
            msgbox.fadeIn();
        }
        //Podemos ignorar ou tratar como warning, discutivel.
        if(notification.unknow)
        {
            msgbox = $( pannel + " .error", context);
            msgbox.find('span').text(notification.unknow.message);
            msgbox.fadeIn();
        }
        if(notification.system)
        {
            msgbox = $( pannel + " .error", context);
            msgbox.find('span').text(notification.system.message);
            msgbox.fadeIn();
        }
    }

    if(notification.success)
    {
        if(options.onSuccess){
            try{options.onSuccess(response)}catch(_E){};
        }
    }

    if(notification.warning)
    {
        if(options.onWarning){
            try{options.onWarning(response)}catch(_E){};
        }
    }

    if(notification.error || notification.unknow || notification.system)
    {
        if(options.onError){
            try{options.onError(response)}catch(_E){};
        }
    }

    if(options.callback)
    {
        try{options.callback(response)}catch(_E){};
    }

    if(options.redirect)
    {
        location.href = options.redirect;
    }

    if(options.reload && options.reload === true)
    {
        location.reload();
    }

    if(options.closeTimeout)
    {
        window.DirectUI.notificationTimeoutId = window.setTimeout(
                window.DirectUI.closeNotifications,
                options.closeTimeout*1000
            );
    }

}

window.DirectUI.closeNotification = function(element)
{
    $(element).parent().fadeOut();
    if(window.DirectUI.notificationTimeoutId)
    {
        clearTimeout(window.DirectUI.notificationTimeoutId);
    }
    window.DirectUI.notificationTimeoutId = 0;
}

window.DirectUI.closeNotifications = function()
{
    $("#message-panel .message, .m-message-panel .message").hide();
    if(window.DirectUI.notificationTimeoutId)
    {
        clearTimeout(window.DirectUI.notificationTimeoutId);
    }
    window.DirectUI.notificationTimeoutId = 0;
}

window.DirectUI.confirmUnbind = function(){
    $(document.querySelectorAll("#confirmation .confirmation-negative, #confirmation .confirmation-positive")).off('click');
}

window.DirectUI.confirm = function(options)
{
    var defaultOptions = {title:'',message:''}
    options = $.extend({}, defaultOptions, options);

    $("#confirmation-title").text(options.title);
    $("#confirmation-message").text(options.message);
    var csstop = (window.innerHeight / 2) - $("#confirmation").height();
    csstop = csstop > 0 ? csstop: 0;
    $("#confirmation").data('css-top',csstop).foundation('reveal', 'open');
    
    if(options.onSuccess)
    {
        $("#confirmation .confirmation-positive").on('click',function(){
            try{options.onSuccess(options)}catch(_E){};
            $("#confirmation").foundation('reveal', 'close');
            window.DirectUI.confirmUnbind();
        });
    }
    else
    {
        $("#confirmation .confirmation-positive").on('click',function(){
            $("#confirmation").foundation('reveal', 'close');
            window.DirectUI.confirmUnbind();
        });
    }

    if(options.onCancel)
    {
        $("#confirmation .confirmation-negative").on('click',function(){
            try{options.onCancel(options)}catch(_E){};
            $("#confirmation").foundation('reveal', 'close');
            window.DirectUI.confirmUnbind();
        });
    }
    else
    {
        $("#confirmation .confirmation-negative").on('click',function(){
            $("#confirmation").foundation('reveal', 'close');
            window.DirectUI.confirmUnbind();
        });
    }
}

