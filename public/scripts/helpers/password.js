define([

    'jquery',
    'foundation'

], function ($) {

    var init = function () {

        passwordChangeForm();

    };

    var passwordChangeForm = function () {

        $('.password-change-modal-link').on('click', function () {
            $('#password-change-modal').foundation('reveal', 'open');
            return false;
        });

        $('#password-change-form').on('submit', function () {

            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json'
            }).done(function (msg) {
                alert(msg.result);
                if (msg.status === 1)
                {
                    location.reload();
                }
            });

            return false;

        });

    };

    return {
        init: init
    };

});
