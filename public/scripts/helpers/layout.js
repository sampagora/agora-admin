define([

    'jquery',
    'customScroll',
    'helpers/billing',
    'helpers/password',
    'helpers/datatable',
    'jsCookie',
    'helpers/directui'

], function ($, customScroll, Billing, Password, Cookies) {
    
    var headerHeight = 0, bodyHeight = 0, contentHeight = 0;

    var init = function () {

        var ml = document.querySelector(".message.loading");

        $(document).ajaxSend(function() {
            $(ml).fadeIn("fast");
        });

        $(document).ajaxComplete(function() {
            $(ml).fadeOut("fast");
        });

        $(window).on('beforeunload', function(){
            $(ml).fadeIn("fast");
        });

        userDropdown();
        accordion();
        updateContainerHeightForCustomScroll();
        contentScroll();

        jQuery.fn.getPath = function () {
            if (this.length != 1)
            {
                throw 'Elemento inválido';
            }

            var path, node = this, realNode, name, parent, siblings;
            while (node.length)
            {
                realNode = node[0], name = realNode.localName;
                if (!name)
                {
                    break
                };
                name = name.toLowerCase();
                parent = node.parent();

                siblings = parent.children(name);
                if (siblings.length > 1)
                {
                    name += ':eq(' + siblings.index(realNode) + ')';
                }

                path = name + (path ? '>' + path : '');
                node = parent;
            }

            return path;
        };


        $(".sub-menu, .common > a").on("mousedown", function(e){localStorage.setItem("mlastItem", $(this).getPath());});

        Billing.init();
        Password.init();

        $(ml).fadeOut("fast");

        Number.prototype.formatCurrency = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));
            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };

        String.prototype.unformatCurrency = function() {
            return parseFloat(this.replace('.','').replace(',','.'));
        };

        try {
            if (window.location.href.indexOf($(localStorage.getItem("mlastItem")).attr("href")) > 0) {
                $(localStorage.getItem("mlastItem")).addClass("active").closest("div").find(".expand-link").trigger("click");
            }
        } catch(_e) {}
    };

    var updateContainerHeightForCustomScroll = function () {

        headerHeight = $('.main-header').height();
        bodyHeight = document.body.offsetHeight;
        contentHeight = parseInt(bodyHeight - headerHeight) - 55;

        var e = document.querySelectorAll(".content-wrapper .content, .content-wrapper .side-navigation");

        $(e).css('height', contentHeight);
    };

    var accordion = function () {
        var e = document.querySelectorAll('.menu-accordion .expand-link');
        $(e).on('click', function () {
            if ($(this).next().is('ul:hidden'))
            {
                $(this).next().slideDown('fast');
                $(this).addClass('active').find('i').removeClass('fa-caret-down').addClass('fa-caret-up');
            }
            else
            {
                $(this).next().slideUp('fast');
                $(this).removeClass('active').find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
            }
        });
    };

    var userDropdown = function () {

        var dropdown = $('.main-header .user-actions');

        $('.main-header .u_detail').on('click', function () {
            if ( dropdown.is(':hidden') )
            {
                dropdown.fadeIn('fast');
            }
            else
            {
                dropdown.fadeOut('fast');
            }
        });

        dropdown.find('a').on('click', function () {
            dropdown.fadeOut('fast');
        });

    };

    var contentScroll = function () {
        $('.content-wrapper .content').mCustomScrollbar({
            axis: 'y',
            scrollbarPosition: 'inside',
            scrollInertia: 300,
            theme: 'dark-3',
            autoHideScrollbar: true
        });
        $('.content-wrapper .side-navigation').mCustomScrollbar({
            axis: 'y',
            scrollbarPosition: 'inside',
            scrollInertia: 300,
            theme: 'minimal-dark',
            autoHideScrollbar: false
        });
    };

    $(window).resize(function () {
        updateContainerHeightForCustomScroll();
    });

    return {
        init: init
    };

});
