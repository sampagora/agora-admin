define([

    'jquery',
    'jqueryUi'

], function ($) {

    // datepicker
    var handleDatePickers = function () {

        $.datepicker.setDefaults({
            isRTL: false,
            autoclose: true,
            todayHighlight: true,
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
            monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            currentText: "Hoje",
            closeText: "Ok",
            prevText: "Mês anterior",
            nextText: "Próximo mês"
        });

        $('.date-picker').datepicker();

        // opções customizadas de cada elemento
        $("input[data-date-picker]").each(function() {
            var options = $(this).data('date-picker');

            for (var item in options) {
                if (item == 'minDate' || item == 'maxDate') {
                    options[item] = new Date(options[item]);
                }
            }

            $(this).datepicker("destroy");
            $(this).datepicker(options);
        });
    }

    return {
        init: function () {
            handleDatePickers();
        }
    };

});
