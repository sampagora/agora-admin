define([

    'jquery'
    
], function ($) {

    var appendMarkupToBody = function () {
        $("<div id='modal-canvas' class='reveal-modal' data-reveal aria-labelledby='modalTitle' aria-hidden='true' role='dialog'><a class='close-reveal-modal' aria-label='Close'>×</a><div class='row'><div class='small-12 column'><div class='detail-box'></div></div></div></div>").appendTo('body');
    };

    var init = function () {
         appendMarkupToBody();
    };

    return {
        init: init
    };

});
