define([

    'jquery',
    'datatables'

], function ($) {

    var init = function (params) {

        var tableSelector = "#datatable", callback = null, activateSort = true, enableSearch = true,
            dom = '<lf<t>ip>';

        if(params)
        {
            callback = params.callback || params.callback;

            if(params.selectors)
            {
                tableSelector += "," + params.selectors;
            }

            if(typeof params.sort !== "undefined")
            {
                activateSort = params.sort;
            }

            if(typeof params.search !== "undefined" && params.search === false)
            {
                enableSearch = false;
            }
            if (params.dom) {
                dom = params.dom;
            }

        }

        //overwrite de funções para ordenação de dados numeros de moeda brasileiro
        jQuery.fn.dataTableExt.oSort['numeric-comma-asc'] = function (a, b) {
            //remove the dots (.) from the string and then replaces the comma with a dot
          var x = (a == "-") ? 0 : a.replace(/\./g, "").replace(/,/, ".");
          var y = (b == "-") ? 0 : b.replace(/\./g, "").replace(/,/, ".");
          x = parseFloat(x);
          y = parseFloat(y);
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        };

        jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function (a, b) {
          var x = (a == "-") ? 0 : a.replace(/\./g, "").replace(/,/, ".");
          var y = (b == "-") ? 0 : b.replace(/\./g, "").replace(/,/, ".");
          x = parseFloat(x);
          y = parseFloat(y);
          return ((x < y) ? 1 : ((x > y) ? -1 : 0));
        };

        //numeric comma autodetect
        jQuery.fn.dataTableExt.aTypes.unshift(
            function (sData) {
            //include the dot in the sValidChars string (don't place it in the last position)
              var sValidChars = "0123456789-.,", Char, bDecimal = false;

              /* Check the numeric part */
                for (i = 0 ; i < sData.length ; i++)
                {
                    Char = sData.charAt(i);
                    if (sValidChars.indexOf(Char) == -1)
                    {
                        return null;
                    }

                    /* Only allowed one decimal place... */
                    if (Char == ",")
                    {
                        if (bDecimal)
                        {
                        return null;
                        }
                    bDecimal = true;
                    }
                }

              return 'numeric-comma';
            }
        );

        $(tableSelector).dataTable({
            'order': [[0, 'desc']],
            'bSort' : activateSort,
            'bFilter': enableSearch,
            'dom': dom,
            'language': {
                'sLengthMenu':
                    '<select class="datatable-length">' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                    '</select>',
                "sEmptyTable": '<p class="text-center">Nenhum registro foi encontrado</p>',
                "sZeroRecords": '<h4 class="text-center">Nenhum registro foi encontrado</h4>',
                "sInfo": "<strong>_START_</strong> até <strong>_END_</strong> do total de <strong>_TOTAL_</strong> registros.",
                "sInfoEmpty": "Nenhum registro foi encontrado",
                "sInfoFiltered": "(Filtrado do total de _MAX_ registros)",
                "sSearch": '',
                "sSearchPlaceholder": 'Pesquisar',
                "paginate" : {
                    "previous" : "Anterior",
                    "next" : "Próxima"
                },
            },
            "oSearch": {
                "sSearch": (window.location.hash) ? window.location.hash.substring(1) : ''
            },
            "fnDrawCallback": function(e){
                $(".table-content").show();

                if(enableSearch === false)
                {
                    $('.dataTables_filter, .dataTables_length').remove();
                }

                var table = $("#"+e.sTableId).DataTable();
                $(table.table().container()).find('div.dataTables_paginate, div.dataTables_info').css( 'display', table.page.info().pages <= 1 ? 'none':'block');

                if(callback != null){
                    try{callback();}catch(_e){}
                }
            }

        });

    };

    return {
        init: init
    };

});
