define([

    'jquery',
    'foundation',
    'jqueryMask'

], function ($) {

    var init = function () {

        billingForm();

    };

    var billingForm = function () {

        formMasks();

        $('.billing-modal-link').on('click', function () {

            checkBillingInfo(function () {
                $('#billing-modal').foundation('reveal', 'open');
            });

            return false;

        });

        $('#save_billing_btn').on('click', function () {
              $.ajax({
                  method: 'POST',
                  url: $('#billing-form').attr('action'),
                  data: $('#billing-form').serialize(),
                  dataType: 'json'
              }).done(function (msg) {
                if(msg.status != 1)
                {
                    DirectUI.notificateUser({notification:{error:{message:msg.result}}},{insideModal:true, context:"#billing-modal"});
                    return false;
                }
                else
                {
                    location.reload();
                }
              });

              return false;
          });



    };

    var checkBillingInfo = function (callback) {

        $.ajax({
            method: 'GET',
            url: '/billing/form',
            dataType: 'json'
        }).done(function (response) {

            if ( response.mediaBuyerInfo.length < 0 )
                return false;

            $.each(response.mediaBuyerInfo, function (key, value) {
                $('#billing-form input[name="' + key + '"]').val(value);
            });

            callback();

        });

    };

    var formMasks = function () {

        $('#billing-form #cep').mask('00000-000', {
            reverse: false,
            placeholder: "00000-000"
        });

        $('#billing-form #phone').mask('(00) 0000-0000', {
            reverse: false,
            placeholder: "(DDD) 0000-0000"
        });

    };

    return {
        init: init
    };

});
