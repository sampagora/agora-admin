<?php
return array(
   'cas' => array(
      'env' => 'dev'
   ),
   'http_client' => array(
      'uri' => 'http://api.direct.predicta.local',
      'options' => array(
         'timeout' => 60,
         'sslverifypeer' => false,
         'keepalive' => true,
         'adapter' => 'Zend\Http\Client\Adapter\Socket',
      ),
      'headers' => array(
         'Accept' => 'application/hal+json',
         'Content-Type' => 'application/json',
      ),
   ),
   'direct-service-env' => 'dev',

);
