<?php

return array(
   'http_client' => array(
      'uri' => 'https://directapi.predicta.net',
      'options' => array(
         'timeout' => 60,
         'sslverifypeer' => false,
         'keepalive' => true,
         'adapter' => 'Zend\Http\Client\Adapter\Socket',
      ),
      'headers' => array(
         'Accept' => 'application/hal+json',
         'Content-Type' => 'application/json',
      ),
   ),
   'direct-service-env' => 'homolog',
);
