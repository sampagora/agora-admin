<?php
namespace Application\Summary;

use Zend\Session\Container as Session;

interface SummaryInterface
{
    /**
     * Define o container da sessão
     *
     * @var Session $session
     */
    public function setSession(Session $session);

    /**
     * Obtém a instância do container de sessão
     *
     * @return Zend\Session\Container
     */
    public function getSession();

    /**
     * Adiciona/atualiza um item do resumo
     *
     * @param Int|String $key
     * @param Mixed $value
     *
     * @return self
     */
    public function set($key, $value);

    /**
     * Obtém um item do resumo
     *
     * @param Int|String $key
     * @param Mixed      $default Valor padrão caso a $key não tenha valor
     *
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * Obtém os dados do resumo através de um array
     *
     * @return Array
     */
    public function toArray();

    /**
     * Limpa o resumo
     *
     * @return Void
     */
    public function clear();

}
