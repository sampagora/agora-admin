<?php
namespace Application\Summary;

interface SummaryAwareInterface
{
    /**
     * Define a instância do resumo
     *
     * @var SummaryInterface $summary
     */
    public function setSummary(SummaryInterface $summary);

    /**
     * Obtém a instância do resumo
     *
     * @var Session $session
     */
    public function getSummary();
}
