<?php
namespace Application\Summary;

use Zend\Session\Container as Session;

abstract class AbstractSummary
{
    /**
     * @var Session $session
     */
    protected $session;

    public function __construct(Session $session)
    {
        $this->setSession($session);
    }

    /**
     * {@inheritDoc}
     */
    public function setSession(Session $session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * {@inheritDoc}
     */
    public function set($key, $value)
    {
        return $this->session->offsetSet($key, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function get($key, $default = null)
    {
        $value = $this->session->offsetGet($key);

        if (empty($value) && ! is_null($default)) {
            return $default;
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->session->getManager()->getStorage()->clear($this->session->getName());
    }

}
