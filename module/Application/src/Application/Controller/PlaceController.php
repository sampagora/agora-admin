<?php

namespace Application\Controller;

use Application\Controller\AbstractController;

use Zend\View\Model\ViewModel;
use Application\View\Model\JsonModel;
use AgoraService\Validator\Exception as ValidationException;
use AgoraService\Service\Entity\ImageType as ImageType;

class PlaceController extends AbstractController
{
    use \Application\Model\Traits\Validate;

    /**
     * @Profile(["admin"])
     */
    public function indexAction()
    {
        $places  = $this->getServiceLocator()
                          ->get('AgoraService\Service\Domain\Application\Place')
                          ->fetchAll();
        
        return new ViewModel(array('places' => $places));
    }
    
    /**
     * @Profile(["admin"])
     */
    public function formAction()
    {
        $request = $this->getRequest();
        
        $dataZones = [];
        $place = $site = $facebook = null;
        $placeId = $this->params('id',0);
        
        $zones  = $this->getServiceLocator()
                        ->get('AgoraService\Service\Domain\Application\Zone')
                        ->fetchAll();
        
        if ($placeId > 0) {
            try {
                $place = $this->getServiceLocator()
                              ->get('AgoraService\Service\Domain\Application\Place')
                              ->fetchOne($placeId);
                
                $homepages = $this->getServiceLocator()
                                  ->get('AgoraService\Service\Domain\Application\PlaceHomepage')
                                  ->fetchAllCompleteByPlaceId($placeId);
                
                if(count($homepages)){
                    if(isset($homepages['Site'])){
                        $site = $homepages['Site'];
                    }
                    if(isset($homepages['Facebook'])){
                        $facebook = $homepages['Facebook'];
                    }
                }
                
            } catch (\Exception $ex) {
                return $this->redirect()->toUrl('place');
            }
        }
        
        return new ViewModel(array('place' => $place, 'zones' => $zones, 'site' => $site, 'facebook' => $facebook));
    }
    
    /**
     * @Role(["admin"])
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                    try {
                        $post = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
                        
                        if ($post['file']['error'] == 1){
                            throw new Exception('Erro no envio da image do place.');
                        }
                        
                        $placeId = $post['id'];
                        $zoneId = (int) $post['zone_id'];
                        $postArray = [];
                        $postArray['place'] =   [   'id'        => $placeId,
                                                    'zone_id'   => $zoneId,
                                                    'name'      => $post['name'],
                                                    'address'   => $post['address'],
                                                    'number'    => $post['number'],
                                                    'cep'       => $post['cep'],
                                                    'district'  => $post['district'],
                                                    'city'      => 'São Paulo',
                                                    'state'     => 'sp',
                                                ];
                        
                        if ($placeId > 0){
                            $place = $this->getServiceLocator()
                                              ->get('AgoraService\Service\Domain\Application\Place')
                                              ->fetchOne($placeId);
                            $postArray['place']['image_id'] = $place['image_id'];

                            # Mudando a imagem
                            if($post['file']['error'] == 0){
                                $place = $this->getServiceLocator()
                                              ->get('AgoraService\Service\Domain\Application\Place')
                                              ->fetchOne($placeId);
                                $postArray['image'] = $post['file'];
                                $postArray['image']['image_type_id'] = ImageType::PLACE;
                                $postArray['image']['id'] = $place['image_id'];
                            }
                            
                        }else{
                            if($post['file']['error'] == 0){
                                $postArray['image'] =  $post['file'];
                                $postArray['image']['image_type_id'] = ImageType::PLACE;
                            }
                        }
                        
                        if (!empty($post['site'])||!empty($post['facebook'])){
                            if(!empty($post['site'])){
                                $postArray['homepages'][] = ['homepage_id' => 1,'url'=>$post['site']];
                            }
                            if(!empty($post['facebook'])){
                                $postArray['homepages'][] = ['homepage_id' => 2,'url'=> $post['facebook']];
                            }
                        }
                        
                        $this->getServiceLocator()
                             ->get('AgoraService\Service\Transaction\SavePlace')
                             ->save($postArray);
                        
                        $message = _('Place salvo com sucesso.');

                    } catch (\Exception $ex) {
                        $message = $ex;
                    }

                return new JsonModel(array('notification' => $message));
            }
        } catch (\Exception $ex) {
            throw new Exception(_('Erro ao salvar o place.'));
        }
        
        return $this->redirect()->toRoute('application/place/index');
    }
}
