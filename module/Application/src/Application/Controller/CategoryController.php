<?php

namespace Application\Controller;

use Application\Controller\AbstractController;

use Zend\View\Model\ViewModel;
use Application\View\Model\JsonModel;
use AgoraService\Validator\Exception as ValidationException;

class CategoryController extends AbstractController
{
    use \Application\Model\Traits\Validate;

    /**
     * @Profile(["admin"])
     */
    public function indexAction()
    {
        $categories  = $this->getServiceLocator()
                          ->get('AgoraService\Service\Domain\Application\Category')
                          ->fetchAll();
        
        return new ViewModel(array('categories' => $categories));
    }
    
    /**
     * @Profile(["admin"])
     */
    public function formAction()
    {
        $request = $this->getRequest();
        
        # check if is edit
        $category = null;
        $category_id = $this->params('id',0);
        
        if ($category_id > 0) {
            try {
                $category = $this->getServiceLocator()
                                ->get('AgoraService\Service\Domain\Application\Category')
                                ->fetchOne($category_id);
            } catch (\Exception $ex) {
                return $this->redirect()->toUrl('category');
            }
        }
        
        return new ViewModel(array('category' => $category));
    }
    
    /**
     * @Role(["admin"])
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                    try {
                        $post = $this->getRequest()->getPost()->toArray();
                        $postData = [   'id'   => $post['id'],
                                        'name' => $post['name']];
                        
                        $this->getServiceLocator()
                             ->get('AgoraService\Service\Domain\Application\Category')
                             ->save($postData);
                        
                        $message = _('Categoria salva com sucesso.');

                    } catch (\Exception $ex) {
                        $message = $ex;
                    }

                return new JsonModel(array('notification' => $message));
            }
        } catch (\Exception $ex) {
            throw new Exception(_('Erro ao salvar categoria.'));
        }
        
        return $this->redirect()->toRoute('application/category/index');
    }
}
