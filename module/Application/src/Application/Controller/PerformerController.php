<?php

namespace Application\Controller;

use Application\Controller\AbstractController;

use Zend\View\Model\ViewModel;
use Application\View\Model\JsonModel;
use AgoraService\Validator\Exception as ValidationException;

class PerformerController extends AbstractController
{
    use \Application\Model\Traits\Validate;

    /**
     * @Profile(["admin"])
     */
    public function indexAction()
    {
        $performers  = $this->getServiceLocator()
                          ->get('AgoraService\Service\Domain\Application\Performer')
                          ->fetchAll();
        
        return new ViewModel(array('performers' => $performers));
    }
    
    /**
     * @Profile(["admin"])
     */
    public function formAction()
    {
        $request = $this->getRequest();
        
        $dataCategories = [];
        $performer = $site = $facebook = null;
        $performerId = $this->params('id',0);
        
        $categories  = $this->getServiceLocator()
                            ->get('AgoraService\Service\Domain\Application\Category')
                            ->fetchAll();
        
        if ($performerId > 0) {
            try {
                $performer = $this->getServiceLocator()
                                  ->get('AgoraService\Service\Domain\Application\Performer')
                                  ->fetchOne($performerId);
                
                $dataCategories = $this->getServiceLocator()
                                       ->get('AgoraService\Service\Domain\Application\PerformerCategory')
                                       ->fetchAllByPerformerId($performerId);
                
                $homepages = $this->getServiceLocator()
                                  ->get('AgoraService\Service\Domain\Application\PerformerHomepage')
                                  ->fetchAllCompleteByPerformerId($performerId);
                
                if(count($homepages)){
                    if(isset($homepages['Site'])){
                        $site = $homepages['Site'];
                    }
                    if(isset($homepages['Facebook'])){
                        $facebook = $homepages['Facebook'];
                    }
                }
                
            } catch (\Exception $ex) {
                return $this->redirect()->toUrl('performer');
            }
        }
        
        return new ViewModel(array( 'performer' => $performer, 
                                    'dataCategories' => $dataCategories, 
                                    'site' => $site, 
                                    'facebook' => $facebook,
                                    'categories' => $categories));
    }
    
    /**
     * @Role(["admin"])
     */
    public function saveAction()
    {
        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                    try {
                        $post = $this->getRequest()->getPost()->toArray();
                        $postArray = [];
                        $postArray['performer'] =   [   'id'        => $post['id'],
                                                        'name'      => $post['name'],
                                                    ];
                        
                        if (!empty($post['site'])||!empty($post['facebook'])){
                            if(!empty($post['site'])){
                                $postArray['homepages'][] = ['homepage_id' => 1,'url'=>$post['site']];
                            }
                            if(!empty($post['facebook'])){
                                $postArray['homepages'][] = ['homepage_id' => 2,'url'=> $post['facebook']];
                            }
                        }
                        
                        $postArray['categories'] = $post['category'];
                        
                        $this->getServiceLocator()
                             ->get('AgoraService\Service\Transaction\SavePerformer')
                             ->save($postArray);
                        
                        $message = _('Performer salvo com sucesso.');

                    } catch (\Exception $ex) {
                        $message = $ex;
                    }

                return new JsonModel(array('notification' => $message));
            }
        } catch (\Exception $ex) {
            throw new Exception(_('Erro ao salvar o performer.'));
        }
        
        return $this->redirect()->toRoute('application/performer/index');
    }
}
