<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class LoginController extends AbstractActionController
{

    public function indexAction()
    {
        try {
            
            $this->getServiceLocator()->get('auth')->getZendAuthService()->clearIdentity();
            
            $request = $this->getRequest();

            if ($request->isPost()) {
                $identity = $this->checkResult($request);
                return new JsonModel($identity);
            }
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $this->redirect()->toRoute('application');
    }

    private function checkResult($request)
    {
        try {
            
            $result = $this->getServiceLocator()->get('auth')->login($request->getPost('email'), $request->getPost('password'));
            
            $identity = $result->getIdentity();
            $status = 1;
            if (! $result->isValid()) {
                $errors = $result->getMessages();
                throw new \Exception($errors[0]);
            }

        } catch (\Exception $ex) {
            $status = 0;
            $identity = $ex->getMessage();
        }

        return array('status' => $status, 'result' => $identity);
    }

}
