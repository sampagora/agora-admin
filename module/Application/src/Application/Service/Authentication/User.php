<?php
namespace Application\Service\Authentication;

use Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Adapter\Ldap as AdapterLdap,
    Zend\Authentication\AuthenticationService,
    Zend\Authentication\Result;

class User
{

    /**
     * @var Zend\Authentication\AuthenticationService
     */
    protected $zendAuthService;

    /**
     * Construtor
     *
     * @param Zend\Authentication\AuthenticationService $zendAuthService
     */
    public function __construct(AuthenticationService $zendAuthService)
    {
        $this->zendAuthService = $zendAuthService;
    }

    /**
     * Obtém o contexto de \Zend\Authentication\AuthenticationService
     *
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getZendAuthService()
    {
        return $this->zendAuthService;
    }

    /**
     * Realiza o login
     *
     * @param  String $email    Email de login
     * @param  String $password Senha de login
     * @return Result
     */
    public function login($email, $password)
    {
        $auth = $this->getZendAuthService();
        $adapter = $auth->getAdapter();
        
        $adapter->setUsername($email)
                ->setPassword($password);

        return $auth->authenticate();
    }

    /**
     * Obtém os dados do usuário logado
     */
    public function getData()
    {
        return $this->getZendAuthService()->getAdapter()->getData();

    }

}
