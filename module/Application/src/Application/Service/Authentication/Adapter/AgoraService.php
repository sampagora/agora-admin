<?php

namespace Application\Service\Authentication\Adapter;

use Zend\Authentication\Adapter\AbstractAdapter,
    Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Result as AuthenticationResult;
use AgoraService\Service\Domain\Login;

class AgoraService extends AbstractAdapter implements AdapterInterface
{

    /**
     * array Dados do login
     */
    protected $data = array();

    /**
     * @var AgoraService\Service\Domain\Login
     */
    protected $login;
    protected $network;

    public function __construct(Login $login)
    {
        $this->login = $login;
    }

    /**
     * Returns the username of the account being authenticated, or
     * NULL if none is set.
     *
     * @return string|null
     */
    public function getUsername()
    {
        return $this->getIdentity();
    }

    /**
     * Sets the username for binding
     *
     * @param  string $username The username for binding
     * @return Ldap   Provides a fluent interface
     */
    public function setUsername($username)
    {
        return $this->setIdentity($username);
    }

    /**
     * Returns the password of the account being authenticated, or
     * NULL if none is set.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->getCredential();
    }

    /**
     * Sets the password for the account
     *
     * @param  string $password The password of the account being authenticated
     * @return Ldap   Provides a fluent interface
     */
    public function setPassword($password)
    {
        return $this->setCredential($password);
    }

    private function setData(\stdClass $data)
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate()
    {
        $messages = array();

        $username = $this->identity;
        $password = $this->credential;

        // sucesso na autenticação
        try {
            
            $result = $this->login->run($username, $password);
            $this->setData($result);

            return new AuthenticationResult(AuthenticationResult::SUCCESS, $result, $messages);

        } catch (AgoraService\Service\Exception $e) {
            $errorMessage = $e->getMessage();
            $code = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;
            $messages[0] = $errorMessage;

            return new AuthenticationResult($code, $username, $messages);
        } catch (\Exception $e2) {
            $errorMessage = $e2->getMessage();
            $code = AuthenticationResult::FAILURE_UNCATEGORIZED;
            $messages[0] = $errorMessage;

            return new AuthenticationResult($code, $username, $messages);
        }
    }

}
