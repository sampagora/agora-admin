<?php
namespace Application\Service\Authentication\Adapter;

use Zend\Authentication\Adapter\AbstractAdapter,
    Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Adapter\Exception,
    Zend\Authentication\Result as AuthenticationResult;

use DirectSdk\Service\Api\ApiInterface;

class Api extends AbstractAdapter implements AdapterInterface
{

    /**
     * array Dados do login
     */
    protected $data = array();

    /**
     * @var DirectSdk\Service\Api\ApiInterface
     */
    protected $api;

    protected $network;

    /**
     * Construtor
     *
     * @param  string  $resource Nome do recurso do webservice
     * @param  Request $request  Instância
     * @return void
     */
    public function __construct(ApiInterface $api)
    {
        $this->api = $api;
    }

    /**
     * Returns the username of the account being authenticated, or
     * NULL if none is set.
     *
     * @return string|null
     */
    public function getUsername()
    {
        return $this->getIdentity();
    }

    /**
     * Sets the username for binding
     *
     * @param  string $username The username for binding
     * @return Ldap   Provides a fluent interface
     */
    public function setUsername($username)
    {
        return $this->setIdentity($username);
    }

    /**
     * Returns the password of the account being authenticated, or
     * NULL if none is set.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->getCredential();
    }

    /**
     * Sets the password for the account
     *
     * @param  string $password The password of the account being authenticated
     * @return Ldap   Provides a fluent interface
     */
    public function setPassword($password)
    {
        return $this->setCredential($password);
    }

    public function setNetwork($network)
    {
        $this->network = $network;

        return $this;
    }

    private function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate()
    {
        $messages = array();

        $username = $this->identity;
        $password = $this->credential;
        $network  = $this->network;

        if (! $username) {
            $code = AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND;
            $messages[0] = 'Nome de usuário é obrigatório';

            return new AuthenticationResult($code, '', $messages);
        }

        if (! $password) {
            $code = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;

            $messages[0] = 'Senha é obrigatório';

            return new AuthenticationResult($code, '', $messages);
        }

        if (! $network) {
            $code = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;

            $messages[0] = 'A rede é obrigatória';

            return new AuthenticationResult($code, '', $messages);
        }

        try {
            $result = $this->api->login($username, $password, $network);

            // sucesso na autenticação
            if ($result['success']) {
                $this->setData($result['data']);

                $canonicalName = $result['data']['name'];

                $messages[] = "{$canonicalName} authentication successful";

                return new AuthenticationResult(AuthenticationResult::SUCCESS, (object) $result['data'], $messages);
            } else {
                $errorMessage = $result['message'];

                if ('Credenciais inválidas.' == $errorMessage) {
                    $code = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;
                    $messages[0] = $errorMessage;
                } else {
                    $code = AuthenticationResult::FAILURE_UNCATEGORIZED;
                    $messages[0] = $errorMessage;
                }

                return new AuthenticationResult($code, $username, $messages);
            }
        } catch (Exception\RuntimeException $e) {
            throw new Exception\RuntimeException($e->getMessage());
        }

        $msg = isset($messages[1]) ? $messages[1] : $messages[0];
        $messages[] = "$username authentication failed: $msg";

        return new AuthenticationResult($code, $username, $messages);
    }

}
