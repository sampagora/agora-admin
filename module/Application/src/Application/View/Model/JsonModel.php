<?php
namespace Application\View\Model;

use Zend\View\Model\JsonModel as ZendJsonModel;
use Zend\Db\Sql\Expression as DbExpression;
use AgoraService\Validator\Exception as ValidationException;
use AgoraService\Dao\Exception\MapperException;

class JsonModel extends ZendJsonModel
{
    /**
     * Constructor
     *
     * @param  null|array|Exception|String|Traversable $variables
     * @param  array|Traversable $options
     */
    public function __construct($variables = null, $options = null)
    {
        if(isset($variables['notification'])){
            $notification = $variables['notification'];
            if($notification instanceof ValidationException) {
                $result = array();
                switch($notification->getCode()) {
                    case 0:
                        $result['success'] = array("message" => $notification->getMessage());
                        break;
                    case E_ERROR:
                        $result['error'] = array("message" => $notification->getMessage());
                        break;
                    case E_WARNING:
                        $result['warning'] = array("message" => $notification->getMessage());
                        break;
                    default:
                        $result['unknow'] = array('message' => $notification->getMessage(),'code'=>$variables->getCode());
                }
                $variables['notification'] = $result;
                $notification = $variables['notification'];
            }

            if($notification instanceof \Exception) {
                $result = array(
                        'system' => array(
                                "message" => _('Ocorreu um erro no sistema. Entre em contato com o suporte.'),
                                "runtime" => $notification->getMessage(),
                                "exception" => get_class($notification)
                            )
                    );
                $variables['notification'] = $result;
            }

            if(is_string($notification)) {
                $variables['notification'] = array("success" => array("message" => $notification));
            }
        }
        parent::__construct($variables,$options);
    }

}
