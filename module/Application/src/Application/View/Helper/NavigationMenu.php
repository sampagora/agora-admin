<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\JsonModel;

class NavigationMenu extends AbstractHelper
{

    public function __construct()
    {
        $this->navigationCategories = new \stdClass();
        define("MENU_ITEMS_PATH","/../../../../config/navigation/menu/");
    }

    public function __invoke($profile)
    {

        $menu_categories = array(
            "Common" => array("key" => "common", "name" => "", "ico" => null),
            "Reports" => array("key" => "reports", "name" => "Relatórios",  "ico" => null),
            "Configuration" => array("key" => "configuration", "name" => "Configurações", "ico" => null),
            "Offline" => array("key" => "offline", "name" => "Jornal", "ico" => null),
            "Online" => array("key" => "online", "name" => "Digital", "ico" => null),
            "NegotiationRadio" => array("key" => "negotiationRadio", "name" => "Venda Direta Radio", "ico" => null),
            "Auction" => array("key" => "auction", "name" => "Leilão",  "ico" => null),
-           "CreativeApproval" => array("key" => "creativeapproval", "name" => "Aprovação de Criativos",  "ico" => null),
-           "Radio" => array("key" => "radio", "name" => "Rádio",  "ico" => null),
            "CreativeApproval" => array("key" => "creativeapproval", "name" => "Aprovação de Criativos",  "ico" => null),
            "Pacote" => array("key" => "package", "name" => "Pacotes",  "ico" => null)
        );

        foreach($menu_categories as $category)
        {
            $key = $category["key"];
            $this->navigationCategories->{$key} = new \stdClass;
            $this->navigationCategories->{$key}->name = $category["name"];
            $this->navigationCategories->{$key}->ico = $category["ico"];
            $this->navigationCategories->{$key}->items = array();
        }

        $this->getMenuItemsForProfile($profile);

        return $this->navigationCategories;
    }

    protected  function addMenuItem($category,$menuData)
    {
        $this->navigationCategories->{$category}->items[] = $menuData;
    }

    protected function getMenuItemsForProfile($profile)
    {

        $profiles = array(
            'admin' => function()
             {
                $this->addMenuItem ("common", include(__DIR__ . MENU_ITEMS_PATH . 'dashboard.php'));
                $this->addMenuItem ("common", include(__DIR__ . MENU_ITEMS_PATH . 'category.php'));
                $this->addMenuItem ("common", include(__DIR__ . MENU_ITEMS_PATH . 'place.php'));
                $this->addMenuItem ("common", include(__DIR__ . MENU_ITEMS_PATH . 'performer.php'));
             },
       );


        $profiles[$profile]();
    }

}
