<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Navigation;

use Zend\Navigation\Service\AbstractNavigationFactory;

/**
 * Default navigation factory.
 */
class Navigation extends AbstractNavigationFactory
{

    protected $serviceName;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->serviceName;
    }

    public function setName($name)
    {
        $this->serviceName = $name;
    }
}
