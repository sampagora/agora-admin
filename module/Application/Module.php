<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        // Aplicação
        $app = $e->getApplication();
        // Event manager
        $em = $app->getEventManager();
        // Service Manager
        $sm = $app->getServiceManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($em);

        $translator = $sm->get('translator');

        // Define traduções default para mensagens de validação e captcha
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        \Zend\Captcha\AbstractAdapter::setDefaultTranslator($translator);

        $this->restrictAccess($e);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
           'Zend\Loader\StandardAutoloader' => array(
              'namespaces' => array(
                 __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
              ),
           ),
        );
    }

    private function restrictAccess(\Zend\Mvc\MvcEvent $e)
    {

        // Aplicação
        $app = $e->getApplication();
        // Event manager
        $em = $app->getEventManager();
        // Service Manager
        $sm = $app->getServiceManager();
        // recuperando authService
        $authService = $sm->get('zend.auth.service');

        $em->attach(\Zend\Mvc\MvcEvent::EVENT_ROUTE, function ($e) use ($authService) {

            # recupera declaração do método a ser executado.
            $match = $e->getRouteMatch();
            $params = $match->getParams();
            try {
                $class = new \ReflectionClass("\\" . $params['controller'] . "Controller");
                $methodDeclaration = $class->getMethod(\Zend\Mvc\Controller\AbstractController::getMethodFromAction($params['action']))->__toString();
            } catch (\ReflectionException $re) {
                return;
            }

            try {

                $this->checkAcl($methodDeclaration, $authService);

            } catch (\Exception $ex) {

                $e->stopPropagation(true);

                $request = $e->getRequest();

                if ($request->isXmlHttpRequest()) {
                    $output = array('status' => 0, 'result' => $ex->getMessage());

                    return new \Zend\View\Model\JsonModel($output);
                }
                $response = $e->getResponse();
                $response->getHeaders()->addHeaderLine('Location', '/');
                $response->setStatusCode(302);

                return $response;
            }
        }, -100);
    }

    private function checkAcl($methodDeclaration, $authService)
    {
        # procura por alguma regra de ACL de profile
        $matches = [];
        $int = preg_match('/@Profile\((.*?)\)/i', $methodDeclaration, $matches);

        # se não achar regra de ACL, página aberta, só prosseguir e abandona checagem
        if (!$int) {
            return;
        }

        # temos ACL. Logo checamos se existe identity na sessão, antes de checar as permissões da ACL.
        if (!$authService->hasIdentity()) {
            throw new \Exception('Sessão Expirada.');
        }

        # temos identity e uma ACL. Vamos checar as permissões
        $identity = $authService->getIdentity();
        $profileAcl = json_decode($matches[1]);

        # se a acl de profile for para todos, ja libera a saida.
        if ('all' === $profileAcl) {
            return;
        }

        # verificando se a ACL é de usuário logado. Se for, já libera, independente do perfil.
        $this->checkProfile($identity->profile, $profileAcl);

        $this->checkRoleAcl($methodDeclaration, $identity);
    }

    private function checkProfile($profile, $profileAcl)
    {
        if (!is_array($profileAcl)) {
            throw new \Exception('Perfil não identificado.');
        }

        if (!in_array($profile, $profileAcl, 1)) {
            throw new \Exception("Perfil não autorizado.");
        }
    }

    private function checkRoleAcl($methodDeclaration, $identity)
    {

        # perfil checado. Vamos ver se o papel(Role) do perfil tem permissão
        $matches = [];
        $int = preg_match('/@Role\((.*?)\)/i', $methodDeclaration, $matches);

        if (!$int) {
            return;
        }

        $roleAcl = json_decode($matches[1]);

        if (!is_array($roleAcl)) {
            throw new \Exception('Papel/Role não identificado.');
        }

        if (!in_array($identity->role, $roleAcl, 1)) {
            throw new \Exception("Você não tem permissão para executar esta ação.");
        }
    }

}
