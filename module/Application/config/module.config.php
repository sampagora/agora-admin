<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
   'router' => array(
      'routes' => array(
         // The following is a route to simplify getting started creating
         // new controllers and actions without needing to create a new
         // module. Simply drop new controllers in, and you can access them
         // using the path /application/:controller/:action
         'application' => array(
            'type' => 'Literal',
            'options' => array(
               'route' => '/',
               'defaults' => array(
                  '__NAMESPACE__' => 'Application\Controller',
                  'controller' => 'Index',
                  'action' => 'index',
               ),
            ),
            'may_terminate' => true,
            'child_routes' => array(
               'default' => array(
                  'type' => 'Segment',
                  'options' => array(
                     'route' => '[:controller[/:action]][/:id]',
                     'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-z0-9]*',
                     ),
                     'defaults' => array(
                     ),
                  ),
               )
            ),
         ),
      ),
   ),
   'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
            'Application\Model\AbstractFactory\ModelManager',
            'Application\Navigation\NavigationAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
            'Zend\Authentication\AuthenticationService' => 'zend.auth.service',
        ),
        'factories' => array(
            'auth' => function ($sm) {
                // storage para armazenar dados do login
                $storage = new Zend\Authentication\Storage\Session();

                // meio de autenticação
                $adapter = new Application\Service\Authentication\Adapter\AgoraService($sm->get('AgoraService\Service\Domain\Login'));

                // serviço de autenticação do Zend
                $authService = new Zend\Authentication\AuthenticationService($storage, $adapter);

                // serviço de autenticação customizado para os usuários do sampagora
                return new Application\Service\Authentication\User($authService);
            }
        ),
        'invokables' => array(
            'zend.auth.service' => 'Zend\Authentication\AuthenticationService',
            'zend.validator.file.image-size' => 'Zend\Validator\File\ImageSize',
            'zend.validator.file.upload-file' => 'Zend\Validator\File\UploadFile',
            'zend.validator.file.size' => 'Zend\Validator\File\Size'
        ),
   ),
   'translator' => array(
        'locale' => 'pt_BR',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../languages',
                'pattern' => '%s.mo',
                'text_domain' => 'default'
            ),
            array(
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../languages',
                'pattern' => '/%s/Zend_Validate.php',
                'text_domain' => 'default',
            ),
            array(
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../languages',
                'pattern' => '/%s/Zend_Captcha.php',
                'text_domain' => 'default',
            ),
        ),
    ),
   'controllers' => array(
      'invokables' => array(
         'Application\Controller\Index' => 'Application\Controller\IndexController',
         'Application\Controller\Login' => 'Application\Controller\LoginController',
         'Application\Controller\Dashboard' => 'Application\Controller\DashboardController',
         'Application\Controller\Category' => 'Application\Controller\CategoryController',
         'Application\Controller\Place' => 'Application\Controller\PlaceController',
         'Application\Controller\Performer' => 'Application\Controller\PerformerController',
          ),
   ),
   'view_manager' => array(
      'display_not_found_reason' => true,
      'display_exceptions' => true,
      'doctype' => 'HTML5',
      'not_found_template' => 'error/404',
      'exception_template' => 'error/index',
      'template_path_stack' => array(
         __DIR__ . '/../view',
      ),
      'strategies' => array(
         'ViewJsonStrategy',
      ),
   ),

    'template_map' => array(
        'layout/generic' => __DIR__ . '/../view/layout/generic.phtml'
    ),

   'view_helpers' => array(
      'invokables' => array(
         'navigationMenu' => 'Application\View\Helper\NavigationMenu',
      ),
   )
);
