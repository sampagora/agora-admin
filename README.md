# Configurando o New Direct

## Instalação de dependências, rodar
    php composer.phar install
    
## Configurar o arquivo hosts na sua máquina
192.168.33.10 direct.predicta.dev direct.dev marketplace.dev new.direct.predicta.dev api.dev direct-api.dev dev.direct.predicta.dev homolog.direct.predicta.dev anj.direct.predicta.dev

## Instalar Vagrant (https://www.vagrantup.com) 

## Baixar o projeto Direct Vagrant em git@gitlab.corp.predicta.com.br:direct/vagrant.git

## Inicializar a máquina virtual a partir do projeto com "vagrant init"

## Adicionar maquina ao Vagrant: "vagrant box add predicta/direct vagrant/direct-vagrant-base-box-v1.box"

## Um arquivo "Vagrantfile" será criado na mesma pasta, editar ele identificando a chave "config.vm.synced_folder" e Preencher com o seguinte conteúdo:
config.vm.synced_folder "PATH DO DIRETÓRIO ONDE SE ENCONTRA O AQUIVIO direct-vagrant-base-box-v1.box", "/var/www/html", create: true
## EXEMPLO: config.vm.synced_folder "/Users/me/direct/vagrant", "/var/www/html", create: true

## Rodar vagrant up para iniciar a máquina virtual os arquivos do projeto new direct automaticamente são carregados pela máquina virtual

## Rodar "vagrant ssh" para poder acessar a máquina recém criada e atualizar o arquivo de configuração do Apache com o conteúdo abaixo

### new-direct.conf no Apache
   <VirtualHost *:80>
       DocumentRoot /var/www/html/new-direct/public
       ServerName direct.dev.predicta.local
       ServerAlias *.dev.predicta.local
       setEnv APPLICATION_ENV development

       DirectoryIndex index.php index.html

       <Directory /var/www/html/new-direct/public/>
           AllowOverride All
           Order Allow,Deny
           Allow from all
       </Directory>
   </VirtualHost>


# Frontend

## Requisitos

- [NodeJS](https://nodejs.org/en/)
- [Gulp](http://gulpjs.com/)
- [Bower](http://bower.io/)

Após a instalação de todos os requisitos acima, execute o passo a passo abaixo:

    cd ./public
    npm install && bower install

## Gulp

Execute o comando abaixo para compilar o css automaticamente.

    gulp sass

Para gerar um novo tema:

- Navegue até o diretório ./public/styles/scss/themes, duplique o arquivo de configuração de algum tema que já existe;

- Renomeie o arquivo duplicado utilizando o mesmo nome da network que fará o uso do tema;

- Edite as variáveis do arquivo de configuração;

Rode o comando abaixo para gerar o novo tema:

    gulp generator --sassFile="nome-da-network.direct.predicta.net" (sem as aspas)