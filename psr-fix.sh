#!/bin/bash

php_cs_fixer='php-cs-fixer.phar'

if [ ! -e $php_cs_fixer ]; then
    echo -e "\nBaixando PHP CS Fixer\n"
    wget http://cs.sensiolabs.org/get/php-cs-fixer.phar
else
    echo "Atualizando PHP CS Fixer"
    php php-cs-fixer.phar self-update
fi

echo -e "\nExecutando PHP CS Fixer\n"
php php-cs-fixer.phar fix . --level=all

